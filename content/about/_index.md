---
title: ABOUT ME
description: Hey, I'm Anna.
---

*“We can only see a short distance ahead, but we can see plenty there that needs to be done.”* -Alan Turing<br><br>

I’m a Computer Science student at Illinois State University, graduating in May 2024. My interests are web development, cloud computing, containerization, and learning new technologies. I am an AWS Certified Cloud Practitioner, and am working towards obtaining my Terraform Associate certification. <br>

In my free time I enjoy cooking, weightlifting, and traveling. I have loved technology from a young age and am excited to continue my journey.<br><br><br>

![little anna](/images/little-anna.jpg)
*(Anna, 1999)*
