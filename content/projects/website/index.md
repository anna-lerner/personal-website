---
title: Personal Website
date: "2022-09-15T19:47:09+02:00"
jobDate: September 2022
work: [web-design, devops]
techs: [hugo, aws, s3, route53, terraform]
thumbnail: website/hugo.svg
---
This website is my first major project. It's a work in progress so check back often to see the improvements I'm making!<br>

## Website
I built the site using [Hugo](https://gohugo.io/) along with a customized theme. 

## Infrastructure 
The website is hosted in an [AWS S3](https://aws.amazon.com/s3/) bucket with [Route 53](https://aws.amazon.com/route53/) setup for routing traffic from my domain name.

I used [Terraform](https://www.terraform.io/) to provision all of the infrastructure for the site. The Terraform code can be found [here](https://gitlab.com/anna-lerner/personal-website/-/tree/main/terraform).

## Pipeline
The site is deployed to the S3 bucket using a GitLab CI/CD pipeline.<br>
This pipeline builds the website content and then uploads it to the bucket.<br>
Source code for the pipeline can be found [here](https://gitlab.com/anna-lerner/personal-website/-/blob/main/.gitlab-ci.yml).
